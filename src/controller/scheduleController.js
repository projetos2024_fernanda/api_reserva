const connect = require("../db/connect");
module.exports = class scheduleController {
  static async createSchedule(req, res) {
    const {
      id,
      dateStart,
      dateEnd,
      days,
      user,
      classroom,
      timeStart,
      timeEnd,
    } = req.body;

    if (
      !dateStart ||
      !dateEnd ||
      !days ||
      !user ||
      !classroom ||
      !timeStart ||
      !timeEnd
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos!" });
    }

    

    const daysString = days.map((day) => `${day}`).join(",");
    //para transformar um array em uma string, mapear e pegar um dia com uma vírgula junto

    //verficando se já não existe uma reserva
    try {
      const orverlapQuery = ` select * from schedule WHERE classroom = '${classroom} '
        AND
        (
            (dateStart = '${dateStart}' AND dateEnd = '${dateEnd}' )
        )
        AND
        (
            (timeStart = '${timeStart}' AND timeEnd = '${timeEnd}' )
        ) 
        AND
        (
            (days LIKE '%Seg%' AND '${daysString}' LIKE '%Seg%')OR
            (days LIKE '%Ter%' AND '${daysString}' LIKE '%Ter%')OR
            (days LIKE '%Qua%' AND '${daysString}' LIKE '%Qua%')OR
            (days LIKE '%Qui%' AND '${daysString}' LIKE '%Qui%')OR
            (days LIKE '%Sex%' AND '${daysString}' LIKE '%Sex%')OR
            (days LIKE '%Sab%' AND '${daysString}' LIKE '%Sab%')
        ) `;

      connect.query(orverlapQuery, function (err, results) {
        if (err) {
          res
            .status(500)
            .json({ err: "Erro ao verificar agendamento existente" });
        }

        //se houver resultado e já existir agendamento
        if (results.length > 0) {
          res
            .status(400)
            .json({ err: "Sala ocupada para os mesmos dias e/ou horários" });
        }

        const insertQuery = `INSERT into schedule (dateStart, dateEnd, days, user, classroom, timeStart, timeEnd) VALUES
            (
                '${dateStart}',
                '${dateEnd}',
                '${daysString}',
                '${user}',
                '${classroom}',
                '${timeStart}',
                '${timeEnd}'
                
            )`;

        //executando a query de inserção
        connect.query(insertQuery, function (err, results) {
          if (err) {
            res.status(500).json({ err: "Erro ao cadastrar agendamento" });
          }
          return res
            .status(201)
            .json({ err: "Agendamento cadastrado com sucesso" });
        });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta");
      res.status(500).json({ err: "Erro interno do servidor" });
    }
  } // fim da createSchedule
}; // fim do modules exports
