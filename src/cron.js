const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpScheduleServices");

//Agendamento da limpeza

// os * é onde coloca regra de agendamento(data, minuto, hora)
cron.schedule("*/30 * * * * *", async () => {
  try {
    await cleanUpSchedules();
    console.log("Limpeza automática executada!");
  } catch (error) {
    console.error("Erro ao executar limpeza automática", error);
  }
});
