const connect = require("../db/connect");

async function cleanUpSchedules(){
    const currentDate = new Date(); 
    
    
    // seta a data, com o valor atual, menos os dias que quero que passe
    //definir  a data para 7 dias atrás
    currentDate.setDate(currentDate.getDate() - 7)
    const fomattedDate = currentDate.toISOString().split('T')[0];//formata a data para YYYY-MM-DD


    const query = `DELETE from schedule WHERE dateEnd < ?`
    const values = [fomattedDate];

    return new Promise ((resolve, reject) => {
        connect.query(query ,values, function(err, results) {
            if(err){
                console.log("Erro MYSQL", err)
                return reject (new Error("Erro ao limpar agendamento "));
            }
            console.log("Agendamento antigos apagados!")
            resolve ("Agendamentos antigos limpos com sucesso!!")
        })
    });
}
module.exports= cleanUpSchedules;